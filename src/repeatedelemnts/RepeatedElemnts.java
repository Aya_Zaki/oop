


package repeatedelemnts;

import java.util.Scanner;
import java.util.HashSet;
import java.util.Arrays;

public class RepeatedElemnts {

  
    public static void main(String[] args) {
         

            String[] Fruits;
            
            //Array WITH Duplicated Elemnts:
            Fruits = new String[] {"Apple","Orange","Banana","Strawberry","Orange","Watermellon","Apple","Banana","Apple","Grape","Apple"};
            
            //Array WITHOUT Duplicated Elemnts:  
            //Fruits = new String[] {"Apple","Orange","Banana"};

           System.out.println("Enter 1 to find Duplicate elemnts using O(n)");
           System.out.println("Enter 2 to find Duplicate elemnts using O(nlogn)");
           System.out.println("Enter 3 to find Duplicate elemnts using O(n*n)");

           Scanner scanner = new Scanner(System.in);
           int Num = scanner.nextInt();
           
           boolean NoDuplicateElemnts = true;
           
           HashSet<String> myHash = new HashSet<>();
           HashSet<String> myHash2 = new HashSet<>();
           
           switch(Num)
             {
                 case 1: // O(n)
                            if(Fruits == null) return;
                        
                            for(int i = 0; i < Fruits.length; i++) {
                               if(myHash2.contains(Fruits[i])){
                                      myHash.add(Fruits[i]);
                                NoDuplicateElemnts = false;}
                                myHash2.add(Fruits[i]);
                                }  
                            
                            
                              if (NoDuplicateElemnts)
                                    System.out.println("No Duplicate Elemnts!");
                                     else
                                 System.out.println(myHash);
                             
                                     break;

                 case 2: // O(nlogn)

                            if(Fruits == null) return;
                           
                            Arrays.sort(Fruits);
                            for (int i = 1; i < Fruits.length; i++){
                               if (Fruits[i] == Fruits[i - 1]){
                                   myHash.add(Fruits[i]);
                            NoDuplicateElemnts = false;}
                            }
                               if (NoDuplicateElemnts)
                                    System.out.println("No Duplicate Elemnts!");
                                     else
                                 System.out.println(myHash);
                                    
                            break;            

                 case 3: // O(n*n)
                            if(Fruits == null) return;
                            
                            for (int i = 0;i < Fruits.length; i++){     
                                    for (int j = i+1 ;j < Fruits.length; j++){
                                       if(Fruits[i] == Fruits[j]){
                                        myHash.add(Fruits[i]);
                                           
                                    NoDuplicateElemnts = false;}
                                }
                                }
                                
                            if (NoDuplicateElemnts)
                                    System.out.println("No Duplicate Elemnts!");
                                     else
                                 System.out.println(myHash);
                            break;                      

                 default: 
                            System.out.println("Invalid Number");
                            break;

             }

    
}
}
